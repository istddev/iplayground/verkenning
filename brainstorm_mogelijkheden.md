# Brainstorm mogelijkheden

## Hoe zou een GitLab publicatie eruit kunnen zien?

```plantuml
@startmindmap
+ Vormgeving
++ Boekvorm
+++ Explainers
+++ Overig?
++ ResPec
+++ Proces standaarden
+++ Functionele standaarden
+++ Technische standaarden
++ Widoco
+++ Ontologie
++ Overige vormen?
@endmindmap
```

## Waaraan zou een GitLab publicatie aan moeten voldoen?

```plantuml
@startmindmap
+ Requirements
++ Afbakening rollen
+++ Analisten
+++ Reviewers
++ Versie strategie
+++ Wijzigingenbeheer vanuit modelleren?
+++ Preview opties
+++ Workflows onderhoud en publicatie
@endmindmap
```
