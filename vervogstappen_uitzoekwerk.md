# Vervolgstappen uitzoekwerk

```plantuml
@startmindmap
+ Minimale publicatie set
++ Berichten
+++ UML-diagrammen
++++ Migreren inclusief
+++++ Koppeling naar regels
+++++ Koppeling naar gegevenstypen
+++++ Tooling inrichten om te modelleren
+++ XSD
++++ Generatie vanuit UML-diagrammen
++ Procesmodellen
+++ Doorlink binnen procesmodellen
+++ Procesbeschrijvingen
++ Casussen
+++ Envoudige vorm vinden
+++ Gericht op het publiek
++ Regels
+++ Titel
+++ Documentatie
+++ Eigenschappen
+++ Onderlinge relaties
@endmindmap
```

```plantuml
@startmindmap
+ Future publicatie set
++ Casussen
+++ Koppeling naar processen
++ Thesaurus termen doorlink en lijst
++ Doorlink van proces
+++ naar bericht
+++ naar regels
@endmindmap
```
