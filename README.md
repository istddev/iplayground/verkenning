# iVariant A

Verkenning opzet iStandaard publicatie variant A. Deze site is bedoeld als experimenteeromgeving om te komen tot een beeld hoe een (klassieke) iStandaard-publicatie via GitLab zou kunnen worden gerealiseerd.

Doel is om met de PPIM-SA collega's te komen tot een stapsgewijze opzet waarmee we vanuit voortschrijdend inzicht kunnen werken aan een volledig geautomatiseerde publicatie waar:

- Visuele controles zo veel als mogelijk worden vervangen door vergelijkingen op gestandaardiseerde gegevens (RDF, Markdown, e.o. generieke standaard-formaten)

- De onderlinge verbanden tussen verschillende modellen worden geborgd via geautomatiseerde validaties en/of linking op basis van FAIR-principes

- Er een maximale toolonafhankelijkheid onstaat door de publicatie op gestandaardiseerde gegevens te baseren die door meerdere tools (zowel open source als commercieel) kunnen worden ondersteund^[GitLab is dan de enige afhankelijkheid maar in basis ook gebaseerd op Open Source componenten].

- De verschillende onderdelen^[Denk aan modellen, beschrijvingen, regels, presentaties, ..etc.] waaruit een iStandaard (waaronder ook KIK-V) bestaat door verantwoordelijke deskundigen kunnen worden beheerd inclusief geautomatiseerde terugkoppeling bij wijzigingen ^[missende verbanden, ontbrekende termen, technische validiteit, domeinwaarden, naamgevingsconventies, ...etc.]
